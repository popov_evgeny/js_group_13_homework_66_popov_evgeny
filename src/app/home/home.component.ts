import { Component, OnDestroy, OnInit } from '@angular/core';
import { CountryService } from '../shared/country.service';
import { CountryModel } from '../shared/country.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  arrayCountries!: CountryModel[];
  loading = false;
  changeArraySubscription!: Subscription;
  fetchingArraySubscription!: Subscription;

  constructor(private countryService: CountryService) { }

  ngOnInit(): void {
    this.arrayCountries = this.countryService.getCountries();
    this.changeArraySubscription = this.countryService.changeArray.subscribe( array => {
      this.arrayCountries = array;
    });
    this.fetchingArraySubscription = this.countryService.fetchingArray.subscribe( isLoading => {
      this.loading = isLoading;
    });
    this.countryService.fetchCountries()
  }

  ngOnDestroy(): void {
    this.changeArraySubscription.unsubscribe();
    this.fetchingArraySubscription.unsubscribe();
  }

}
