import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CountryModel } from '../shared/country.model';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-county',
  templateUrl: './county.component.html',
  styleUrls: ['./county.component.css'],
  animations: [
    trigger('animCountry', [
      state('show', style({
        opacity: 1
      })),
      state('hide', style({
        opacity: 0
      })),
      transition('show => hide', animate('700ms ease-out')),
      transition('hide => show', animate('1400ms ease-in'))
    ])
  ]
})
export class CountyComponent implements OnInit {
  country: CountryModel | null = null;
  img = '';
  show!: boolean;
  loading!: boolean;


  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.loading = true;
      this.show = false;
      setTimeout(() => {
        this.show = true;
        this.country = <CountryModel>data.country;
        this.img = `http://146.185.154.90:8080/restcountries/data/${this.country.alpha3Code.toLocaleLowerCase()}.svg`;
        this.loading = false;
      }, 1200);
    });
  }

  get stateName() {
    return this.show ? 'show' : 'hide'
  }

  get stateLoading() {
    return this.loading ? 'show' : 'hide'
  }

}
