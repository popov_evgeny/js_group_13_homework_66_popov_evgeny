import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CountyComponent } from './county/county.component';
import { CountryResolverService } from './shared/country-resolver.service';
import { NotFoundComponent } from './not-found.component';
import { NotSelectedComponent } from './not-selected.component';

const routes: Routes = [
  {path: '', component: NotSelectedComponent},
  { path: 'country/:countryCode/info', component: CountyComponent, resolve: {country: CountryResolverService} },
  {path: '**', component: NotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
