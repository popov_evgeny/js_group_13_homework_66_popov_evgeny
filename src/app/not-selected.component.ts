import { Component } from '@angular/core';


@Component({
  selector: 'app-not-found',
  template: `<h1>Not selected country!</h1>`,
  styles: [`
    h1 {
      color: orange;
    }
  `]
})

export class NotSelectedComponent{}
