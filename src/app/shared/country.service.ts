import { Injectable } from '@angular/core';
import { CountryModel } from './country.model';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable()

export class CountryService {
  private arrayCountries: CountryModel[] = [];
  changeArray = new Subject<CountryModel[]>()
  fetchingArray = new Subject<boolean>()

  constructor(private http: HttpClient) {
  }

  getCountries() {
    return this.arrayCountries.slice();
  }

  fetchCountries() {
    this.fetchingArray.next(true);
    this.http.get<CountryModel[]>('http://146.185.154.90:8080/restcountries/rest/v2/all?fields=name;alpha3Code').subscribe(result => {
      this.arrayCountries = result;
      this.changeArray.next(this.arrayCountries.slice());
      this.fetchingArray.next(false);
    }, () => {
      this.fetchingArray.next(false);
    });
  }

  fetchCountry(code: string) {
    return this.http.get<CountryModel>(`http://146.185.154.90:8080/restcountries/rest/v2/alpha/${code}`).pipe(
      map(result => {
        return new CountryModel(
          result.name, result.alpha3Code,
          result.capital, result.region,
          result.population, result.area,
        );
      })
    );
  }

}
