import { Injectable } from '@angular/core';
import { CountryService } from './country.service';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { CountryModel } from './country.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CountryResolverService implements Resolve<CountryModel> {

  constructor(private countryService: CountryService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<CountryModel>{
    const countryCode = <string>route.params['countryCode'];
    return this.countryService.fetchCountry(countryCode);
  }
}
