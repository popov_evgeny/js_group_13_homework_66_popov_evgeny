export class CountryModel {
  constructor(
    public name: string,
    public alpha3Code: string,
    public capital: string,
    public region: string,
    public population: string,
    public area: string
  ) {}
}
